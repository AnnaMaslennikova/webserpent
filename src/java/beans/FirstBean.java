/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import beansClasses.AdensTable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.inject.Named;
//import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import org.apache.commons.lang.StringUtils;
import org.primefaces.component.api.UIColumn;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author Anna2
 */
@ManagedBean(name = "firstBean")
@ViewScoped
public class FirstBean implements Serializable {

    private String nuclideForGraphic = "";
    private ArrayList<String> nuclide_name;
    private LineChartModel animatedModel1 = new LineChartModel();
    private boolean fuelAndNuclideTab = false;
    private int index = 0;
    private boolean showFuelList;
    private String selectedFuel;
    /*выбранный вид топлива*/
    private ArrayList<String> fuels;
    /*список видов топлива*/
    HashMap<String, String> about_nuclides = null;
    /*хэшмэп, где ключ - нуклид, а значение - строка концентраций*/
    ArrayList<String> nuclides;
    /*список нуклидов*/
    private String fileLines;
    /*содержимое ADENS-файла*/

    ArrayList<String> selectedNuclides;
    String selectNuclide;
    ArrayList<String> concentration;
    String selectedConcentration;

    HashMap<String, ArrayList<String>> adensTable;
    private ArrayList<AdensTable> tableAdens;
    private double[][] doubleTableAdens;
    ArrayList<double[]> arrayListDouble;

    private ArrayList<String> cols;
    private String selectedCols;
    private boolean checkSelectedCol = true;

    private ArrayList<String> startCondition;
    ArrayList<Double> listForInsertToFile;

    private AdensTable selectedRow;

    Properties p = new Properties();
    InputStream fs = null;
    static Logger log = Logger.getLogger(FirstBean.class.getName());

    /**
     * Creates a new instance of FirstFormBean
     */
    public FirstBean() {
        getPropertyInformation();
    }

    public String getNuclideForGraphic() {
        return nuclideForGraphic;
    }

    public void setNuclideForGraphic(String nuclideForGraphic) {
        this.nuclideForGraphic = nuclideForGraphic;
    }

    public boolean isFuelAndNuclideTab() {
        return fuelAndNuclideTab;
    }

    public void setFuelAndNuclideTab(boolean fuelAndNuclideTab) {
        this.fuelAndNuclideTab = fuelAndNuclideTab;
    }

    public boolean isCheckSelectedCol() {
        return checkSelectedCol;
    }

    public void setCheckSelectedCol(boolean checkSelectedCol) {
        this.checkSelectedCol = checkSelectedCol;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public LineChartModel getAnimatedModel1() {
        return animatedModel1;
    }

    public void setAnimatedModel1(LineChartModel animatedModel1) {
        this.animatedModel1 = animatedModel1;
    }

    public AdensTable getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(AdensTable selectedRow) {
        this.selectedRow = selectedRow;
    }

    public String getSelectedFuel() {
        return selectedFuel;
    }

    public void setSelectedFuel(String selectedFuel) {
        this.selectedFuel = selectedFuel;
    }

    public String getSelectedCols() {
        return selectedCols;
    }

    public void setSelectedCols(String selectedCols) {
        this.selectedCols = selectedCols;
    }

    public ArrayList<String> getCols() {
        return cols;
    }

    public void setCols(ArrayList<String> cols) {
        this.cols = cols;
    }

    public double[][] getDoubleTableAdens() {
        return doubleTableAdens;
    }

    public void setDoubleTableAdens(double[][] doubleTableAdens) {
        this.doubleTableAdens = doubleTableAdens;
    }

    public ArrayList<String> getStartCondition() {
        return startCondition;
    }

    public void setStartCondition(ArrayList<String> startCondition) {
        this.startCondition = startCondition;
    }

    public String getSelectedConcentration() {
        return selectedConcentration;
    }

    public void setSelectedConcentration(String selectedConcentration) {
        this.selectedConcentration = selectedConcentration;
    }

    public String getSelectNuclide() {
        return selectNuclide;
    }

    public void setSelectNuclide(String selectNuclide) {
        this.selectNuclide = selectNuclide;
    }

    public ArrayList<String> getConcentration() {
        return concentration;
    }

    public void setConcentration(ArrayList<String> concentration) {
        this.concentration = concentration;
    }

    public ArrayList<AdensTable> getTableAdens() {
        if ((tableAdens != null) && (tableAdens.size() > 1)) {
            log.info("adens = " + tableAdens.get(1).getField5());
        }
        return tableAdens;
    }

    public void setTableAdens(ArrayList<AdensTable> tableAdens) {
        this.tableAdens = tableAdens;
    }

    public ArrayList<String> getSelectedNuclides() {
        return selectedNuclides;
    }

    public void setSelectedNuclides(ArrayList<String> selectedNuclides) {
        this.selectedNuclides = selectedNuclides;
    }

    public HashMap<String, String> getAbout_nuclides() {
        return about_nuclides;
    }

    public void setAbout_nuclides(HashMap<String, String> about_nuclides) {
        this.about_nuclides = about_nuclides;
    }

    public ArrayList<String> getNuclides() {
        return nuclides;
    }

    public void setNuclides(ArrayList<String> nuclides) {
        this.nuclides = nuclides;
    }

    public String getFileLines() {
        return fileLines;
    }

    public void setFileLines(String fileLines) {
        this.fileLines = fileLines;
    }

    public boolean isShowFuelList() {
        return showFuelList;
    }

    public void setShowFuelList(boolean showFuelList) {
        this.showFuelList = showFuelList;
    }

    public ArrayList<String> getFuels() {
        return fuels;
    }

    public void setFuels(ArrayList<String> fuels) {
        this.fuels = fuels;
    }

//    public void populateColumns() {
//        String[] columnKeys = new String[20];
//            for(int i=0; i<20; i++){
//                int ii = i+1;
//                String k = "field" + ii;
//                columnKeys[i] = k;
//                log.info("col key = "+columnKeys[i]);
//            }
//        for (String columnKey : columnKeys) {
//            columns.add(new ColumnModel(columnKey.toUpperCase(), columnKey ));
//        }
//    }
    public void createFuelList() {
        ArrayList<String> fuels_list = new ArrayList<>();
        try {
            String pythonScriptPath = p.getProperty("fuelGetter");
            String[] cmd = {"python", pythonScriptPath};

            // create runtime to execute external command
            ProcessBuilder pb = new ProcessBuilder(cmd);
            // retrieve output from python script       
            pb.redirectError();

            Process p = null;
            InputStream stdout = null;
            InputStreamReader isr = null;
            BufferedReader br = null;
            try {
                p = pb.start();
                stdout = p.getInputStream();
                try {
                    isr = new InputStreamReader(stdout);
                    br = new BufferedReader(isr);
                    String fuel_line = "";
                    while ((fuel_line = br.readLine()) != null) {
                        log.info(fuel_line);
                        int start = fuel_line.indexOf("_") + 1;
                        int end = fuel_line.lastIndexOf("_");
                        String line = fuel_line.substring(start, end);
                        fuels_list.add(line);
                    }
                    br.close();
                    isr.close();
                    stdout.close();

                } catch (Exception e) {
                    e.getMessage();
                }

            } catch (Exception e) {
                e.getMessage();
            }
        } catch (Exception e) {
            e.getMessage();
        }
        fuels = fuels_list;
        log.log(Level.INFO, "[createFuelList] fuels len = " + fuels.size());
        for (String f : fuels) {
            log.log(Level.INFO, f);
        }

    }

    public void getPropertyInformation() {
        try {
            fs = getClass().getClassLoader().getResourceAsStream("paths.properties");
            if (fs == null) {
                log.log(Level.INFO, "[getPropertyInformation] fs is null");
            } else {
                p.load(fs);
            }
        } catch (Exception e) {
            log.log(Level.INFO, "[getPropertyInformation] - ImputStream for paths.properties: " + e.getMessage());
        }
    }

    public void showNuclideList() {
        if (selectedFuel != null) {
            if (selectedFuel.isEmpty() == false) {
                ArrayList<String> nucl = new ArrayList<>();
                HashMap<String, String> about = new HashMap<>();
                //  for (String selFuel : selectedFuel) {
                StringBuffer fileName = new StringBuffer(p.getProperty("pathFuelFile_1"));
                fileName.append(selectedFuel).append(p.getProperty("pathFuelFile_2"));
                log.log(Level.INFO, "Название файла: " + fileName.toString());

                /*читаем текстовый файл для определенного fuel*/
                FileInputStream inputStream = null;
                try {
                    File adensFile = new File(fileName.toString());
                    inputStream = new FileInputStream(adensFile);
                    Scanner sc = new Scanner(inputStream, "UTF-8");
                    nuclides = new ArrayList<>();
                    about_nuclides = new HashMap<String, String>();

                    /*считываем файл построчно*/
                    while (sc.hasNextLine()) {
                        String line = sc.nextLine();
                        if ((line.isEmpty()) || (line.contains("total")) || (line.contains("lost data"))) {
                            continue;
                        }
                        line.trim();
                        String[] line_parts = StringUtils.splitPreserveAllTokens(line, "%");
                        nucl.add(line_parts[1].trim());
                        about.put(line_parts[1].trim(), line_parts[0].trim());
                    }
                    nuclides = nucl;
                    about_nuclides = about;
                    log.info("[showNuclideList]nuclide len = " + nuclides.size());
                    log.info("[showNuclideList]about_nuclide len = " + about_nuclides.size());
                    if ((inputStream != null) && (sc != null)) {
                        sc.close();
                        inputStream.close();
                    }

                } catch (Exception e) {
                    e.getMessage();
                }
                //  }
            } else {
                log.info("[showNuclideList] selectedFuel is empty!");
            }
        } else {
            log.info("[showNuclideList]selectedFuel is null");
        }
    }

    public ArrayList<String> takeNuclideList(String fuelname) {
        ArrayList<String> result = new ArrayList<>();
        try {
            StringBuffer fileName = new StringBuffer(p.getProperty("pathFuelFile_1"));
            fileName.append(fuelname).append(p.getProperty("pathFuelFile_2"));
            System.out.println("Название файла: " + fileName.toString());

            /*читаем текстовый файл для определенного fuel*/
            FileInputStream inputStream = null;
            File adensFile = new File(fileName.toString());
            inputStream = new FileInputStream(adensFile);
            Scanner sc = new Scanner(inputStream, "UTF-8");

            //ArrayList<String> nuclides = new ArrayList<>(); //список нуклидов
            // about_nuclides = new HashMap<String, String>();

            /*считываем файл построчно*/
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.isEmpty()) {
                    continue;
                }
                line.trim();
                String[] line_parts = StringUtils.splitPreserveAllTokens(line, "%");
                result.add(line_parts[1].trim());
                // about_nuclides.put(line_parts[1].trim(), line_parts[0].trim());
            }
            if ((inputStream != null) && (sc != null)) {
                sc.close();
                inputStream.close();
            }

        } catch (Exception e) {
            e.getMessage();
        }
        return result;
    }

    public void activateFistTab() {
        fuelAndNuclideTab = true;
    }

    public void cleanAdensTable() {
        if (tableAdens != null) {
            if (tableAdens.isEmpty() == false) {
                tableAdens.clear();
                log.info("[cleanAdensTable]tableAdens len = " + tableAdens.size());
            } else {
                log.info("[cleanAdensTable] tableAdens is empty!");
            }
        } else {
            log.info("[cleanAdensTable]tableAdens is null!");
        }
        if (animatedModel1 != null) {
            //animatedModel1.clear();
            animatedModel1 = null;
            // log.info("[cleanAdensTable]animateModel len = ");
        } else {
            log.info("[cleanAdensTable]animatedModel is null!");
        }
        if (selectedRow != null) {
            selectedRow = null;
            //  selectedRow = new AdensTable();
        } else {
            log.info("[cleanAdensTable]selectedRow is null!");
        }
    }

    public void calcSelectedFuelLen() {
        log.info("selectedFuel's list len = " + selectedFuel.length());
        //  for (String selFuel : selectedFuel) {
        log.info(selectedFuel.length() + " - " + selectedFuel);
        // }
    }

    public void showConcentration() {
        ArrayList<String> massivConcentration = null;
        log.info("z neeeee");
        if (about_nuclides == null) {
            return;
        } else if (selectNuclide != null) {
            log.info("selected nuclides is not null");
            if (selectNuclide.length() != 0) {
                try {
                    //   for (String nuclide : selectedNuclides) {
                    for (Map.Entry entry : about_nuclides.entrySet()) {
                        String neededNuclide = (String) entry.getKey();
                        if (neededNuclide.equals(selectNuclide)) {
                            String value = (String) entry.getValue();
                            String[] details = StringUtils.splitPreserveAllTokens(value.trim(), " ");
                            int count_of_details = details.length;
                            log.info("count of details = " + count_of_details);
                            massivConcentration = new ArrayList<>();
                            for (int i = 0; i < count_of_details - 1; i++) {
                                try {
                                    // double d = Double.parseDouble(details[i]);
                                    // massivConcentration[i] = d;
                                    massivConcentration.add(details[i]);
                                } catch (Exception ex) {
                                    log.info("[showConcentration]" + ex.getMessage());
                                }

                            }
                        }
                    }
                    //  }
                    concentration = massivConcentration;
                    log.info("concentration len = " + concentration.size());

                } catch (Exception ex) {
                    ex.getMessage();
                }

            } else {
                log.info("[showConcentration]selectedNuclides is empty");
            }
        } else {
            log.info("[showConcentration]selectedNuclides is null");
        }
    }

    public void deleteNuclide() {
        try {
            if (selectNuclide != null) {
                if (selectNuclide.length() != 0) {
                    if (nuclides != null) {
                        if (nuclides.size() != 0) {
                            log.info("заше");
                            log.info("before: " + nuclides.size());
                            nuclides.remove(selectNuclide);
                            log.info("after:" + nuclides.size());

                        } else {
                            log.info("Массив нуклидов пуст");
                        }
                    } else {
                        log.info("Список нуклидов - null");
                    }
                } else {
                    log.info("Нуклид не выбран");
                }
            } else {
                log.info("Выбранный нуклид - null");
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }

    public void loadAdensTable() {
        cols = new ArrayList<>();
        int ii = 1;
        for (int j = 0; j < 20; j++) {
            cols.add("f" + ii);
            ii++;
        }
        this.tableAdens = new ArrayList<>();
        adensTable = new HashMap<>();
        //   for (String fuelname : selectedFuel) {
        ArrayList<String> nuclideTable = new ArrayList<>();
        StringBuffer fileName = new StringBuffer(p.getProperty("pathFuelFile_1"));
        fileName.append(selectedFuel).append(p.getProperty("pathFuelFile_2"));
        System.out.println("Название файла для таблицы: " + fileName.toString());
        try {
            /*читаем текстовый файл для определенного fuel*/
            FileInputStream inputStream = null;
            File adensFile = new File(fileName.toString());
            inputStream = new FileInputStream(adensFile);
            Scanner sc = new Scanner(inputStream, "UTF-8");
            arrayListDouble = new ArrayList<>();
            nuclide_name = new ArrayList<>();
            int j = 1;
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                AdensTable aT = new AdensTable();
                if (line.isEmpty()) {
                    continue;
                }
                line.trim();
                String[] line_parts = StringUtils.splitPreserveAllTokens(line, "%");
                String nuclides_in_file = line_parts[1]; /////
                nuclide_name.add(nuclides_in_file);
                String[] columns = StringUtils.splitPreserveAllTokens(line_parts[0].trim(), " ");
                double[] doubleColumn = new double[columns.length];
                log.info("[loadAdensTable]double[] doubleColumn " + doubleColumn.length);
                for (int i = 0; i < columns.length; i++) {
                    double conc = 0.0;
                    String col = "";
                    try {
                        conc = Double.parseDouble(columns[i]);
                        doubleColumn[i] = conc;
                        col = String.format("%6.2E", conc);
                    } catch (Exception ex) {
                        log.info("[loadAdensTable] exception with Parse to Double: "
                                + ex.getMessage() + ", and columns[i]=" + columns[i]);
                    }
                    if (col.isEmpty() == false) {
                        columns[i] = col;
                    }
                }
                arrayListDouble.add(doubleColumn); //добавить дабл массив (1 строка аденс файла) в двумерную матрицу 
                aT.id = j;
                aT.field1 = columns[0];
                aT.field2 = columns[1];
                aT.field3 = columns[2];
                aT.field4 = columns[3];
                aT.field5 = columns[4];
                aT.field6 = columns[05];
                aT.field7 = columns[6];
                aT.field8 = columns[7];
                aT.field9 = columns[8];
                aT.field10 = columns[9];
                aT.field11 = columns[10];
                aT.field12 = columns[11];
                aT.field13 = columns[12];
                aT.field14 = columns[11];
                aT.field14 = columns[13];
                aT.field15 = columns[14];
                aT.field16 = columns[16];
                aT.field17 = columns[16];
                aT.field18 = columns[17];
                aT.field19 = columns[18];
                aT.field20 = columns[19];
                aT.field21 = columns[20];
                aT.field22 = columns[21];
                for (int i = 0; i < columns.length; i++) {
                    nuclideTable.add(columns[i]);

                }
                j++;
                adensTable.put(line_parts[0], nuclideTable);
                tableAdens.add(aT);
            }
            log.info(tableAdens.get(1).field5);
            log.info("atList len = " + tableAdens.size());
            log.info("arrayList of double massiv len = " + arrayListDouble.size());
        } catch (Exception e) {
            log.info("[getAdensTable] Exception in try: " + e.getMessage());
            e.printStackTrace();
        }
        //  }
    }

    public void cancelChoice() {
        try {
            if (selectedNuclides != null) {
                selectedNuclides.clear();
                selectedNuclides = new ArrayList<>();
            }
            if (nuclides != null) {
                nuclides.clear();
                nuclides = new ArrayList<String>();
            }
//            if (selectedFuel != null) {
//                selectedFuel = "";
//            }
            if (concentration != null) {
                concentration.clear();
                concentration = new ArrayList<>();
            }
            if (selectNuclide != null) {
                selectNuclide = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeStartCondition(CellEditEvent event) {
        try {
            startCondition = new ArrayList<>();
            UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
            DataTable tabler = (DataTable) view.findComponent(":form1:tableAdens");
            List<UIColumn> coler = tabler.getColumns();
            // String col_name = event.getColumn().getFacet(col_name).getHeaderText();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writeColumns() {
        if (selectedCols != null) {
            if (selectedCols.length() != 0) {
                try {
                    listForInsertToFile = new ArrayList<>();
                    try {
                        String number = selectedCols.substring(selectedCols.indexOf("f") + 1, selectedCols.length());
                        log.info("[]string number = " + number);
                        int numb = Integer.parseInt(number);

                        for (double[] dmassiv : arrayListDouble) {
                            // Double d = new Double(dmassiv[numb]);
                            listForInsertToFile.add(dmassiv[numb - 1]);
                            log.info("[writeColumns]adding: " + dmassiv[numb - 1]);
                        }
//                        for (Double mm : listForInsertToFile) {
//                            log.info("[]mm = " + mm);
//                        }

                    } catch (Exception e) {
                        log.info("[writeColumns]Exception: " + e.getMessage());
                        e.printStackTrace();
                    }
                    /*мы получили массив, который отвечает за колонку, теперь надо 
                      вставить в текстовый файл начальных условий*/

                    Double sum = 0.0;
                    sum = this.summator(listForInsertToFile);
                    String summa = String.format("%6.3E", sum);
                    double vol = 0.0;
                    String volstr = "vol";
                    StringBuffer strBuf = new StringBuffer("mat ");
                    String substr = strBuf.toString();
                    strBuf.append(selectedFuel);
                    strBuf.append("    ").append(summa).append("    ").append(volstr)
                            .append(" ").append(vol).append("    burn 1");
                    strBuf.append(System.getProperty("line.separator"));
                    System.out.println("substr = " + substr);

                    BufferedReader reader = new BufferedReader(new FileReader(p.getProperty("pathtoTXT")));
                    String line = null;
                    StringBuilder stringBuilder = new StringBuilder();
                    String ls = System.getProperty("line.separator");
                    int pos = 0;
                    int start = 0;
                    int end = 0;
                    boolean flag = false;

                    String filename1 = "D:\\for_nir\\VVER-Caz22222.txt";
                    /*А писать будем в этот файл*/
                    File txtfilenew = new File(filename1);
                    FileWriter fw = new FileWriter(txtfilenew/*, true*/);

                    /*читаем старый тхт файл и одновременно пишем в новый*/
                    while ((line = reader.readLine()) != null) {
                        pos++;
                        // System.out.println("pos = "+pos);
                        if (line.contains(substr.concat(" "))) {
                            System.out.println("is in start, pos = " + pos);
                            start = pos;
                            flag = true;
                            continue;
                        }
                        if ((flag == true) && (line.contains("fuel"))) {
                            System.out.println("is in end, pos = " + pos);
                            end = pos;
                            flag = false;
                        }
                        if (flag == false) {
                            fw.write(line);
                            fw.write(System.lineSeparator());
                        }
                    }

                    System.out.println("start=" + start);
                    System.out.println("end=" + end);

                    ArrayList<String> nuclidesList = this.takeNuclideList(selectedFuel);
                    for (int i = 0; i < nuclidesList.size() - 1; i++) {
                        if ((nuclidesList.get(i).equals("total") || (nuclidesList.get(i).equals("lost data")))) {

                        } else {
                            strBuf.append("      ").append(nuclidesList.get(i).substring(0, nuclidesList.get(i).length() - 1));
                            strBuf.append(".03c").append(" ").append(listForInsertToFile.get(i));
                            strBuf.append(System.getProperty("line.separator"));
                        }
                    }
                    System.out.println(strBuf.toString());
                    fw.write(strBuf.toString());

                    reader.close();
                    fw.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                log.info("[writeColumns]selectedCols is empty");
            }
        } else {
            log.info("[writeColumns]selectedCols==null");
        }
    }

    public void indexget() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            Map map = context.getExternalContext().getRequestParameterMap();
            String pIndex = (String) map.get("index");
            index = Integer.parseInt(pIndex);
            log.info("[initLinearModel]index = " + index);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private LineChartModel initLinearModel(double[] selRow, String graphicName) {
        LineChartModel model = new LineChartModel();
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("Нуклид " + graphicName);
        if (selRow != null) {
            if (selRow.length > 0) {
                try {
                    for (int i = 0; i < selRow.length; i++) {
                        String setRowStr = String.valueOf(selRow[i]);
                        log.info("setRowStr = " + setRowStr);
                        setRowStr = setRowStr.substring(0, setRowStr.indexOf("."));
                        log.info("substring = " + setRowStr);
                        // setRowStr = setRowStr.replace(".", ",");
                        try {
                            int selRowInt = Integer.parseInt(setRowStr);
                            series1.set(i + 1, selRow[i]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //  log.info("[initLinearModel]series set with selRowi=" + selRow[i]);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                log.info("[initLinearModel]selRow len = 0");
            }
        } else {
            log.info("[initLinearModel]selRow is null");
        }

        model.addSeries(series1);

        return model;
    }

//    public void showNuclideOnGraphic(){
//        if(selectedRow!=null){
//            if(arrayListDouble.get(selectedRow.id - 1) != null){
//                int i =0;
//                for(Map<>)
//            }
//        }
//    }
    public void drawGraphic() {
        if (selectedRow != null) {
            log.info("[drawGraphic]selected row id = " + selectedRow.id);
            int nuclide_number = 0;
            for (String name : nuclide_name) {
                System.out.println("Нуклид =  " + name);
                nuclide_number++;
                if(nuclide_number == selectedRow.id){
                    nuclideForGraphic = name;
                    break;
                }
            }
            if (arrayListDouble.get(selectedRow.id - 1) != null) {
                double[] selRow = arrayListDouble.get(selectedRow.id - 1);
                double sm = selRow[0];
                for (int i = 0; i < selRow.length; i++) {
                    log.info("[drawGraphic] selRow with i = " + i + " equals to " + selRow[i]);
                    if (sm < selRow[i]) {
                        sm = selRow[i];
                    }
                }
//                log.info("max = " + sm);
                if (selRow != null) {
                    animatedModel1 = initLinearModel(selRow, nuclideForGraphic);
                    if (animatedModel1 != null) {
                        animatedModel1.setTitle("Концентрация нуклида: " + nuclideForGraphic);
                        animatedModel1.setAnimate(true);
                        animatedModel1.setLegendPosition("se");
                        Axis yAxis = animatedModel1.getAxis(AxisType.Y);
                        yAxis.setMin(0);
                        yAxis.setMax(sm);
                        yAxis.setTickFormat("%8.4e");
                        Axis xAxis = animatedModel1.getAxis(AxisType.X);
                        xAxis.setMin(0);
                        xAxis.setMax(23);
                    } else {
                        log.info("[drawGraphic] animateModel1 is null");
                    }
                } else {
                    log.info("[drawGraphic] selRow after initLinearModel is null");
                }

            } else {
                log.info("[drawGraphic] arrayListDouble.get(selectedRow.id) = null");
            }

        } else {
            log.info("[drawGraphic]selectedRo is null");
        }
    }

    public Double summator(ArrayList<Double> column) {
        Double sum = 0.0;
        for (Double s : column) {
            sum = sum + s;
        }
        return sum;
    }

    public void trueSetToCheckSelectedCols() {
        checkSelectedCol = false;
    }

}
